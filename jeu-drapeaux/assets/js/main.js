let falgPart = document.getElementsByClassName('card');
let colors = ["white", "red", "blue"]
let pays = document.getElementById("pays");
var span = document.getElementsByClassName("close")[0];




let partie = {
  "level": 0,
  "color1": "aliceblue",
  "color2": "aliceblue",
  "color3": "aliceblue",
  'h': 0,
  'm': 0,
  's': 0,
  'score': 0,
  'scoreFinal': 0,
  'click': 0,
  'clickTotal': 0

}


/** Fonction pour initier le local storage */
window.onload = function () {



  if (localStorage.getItem('partie') != null) {


    partie = JSON.parse(localStorage.getItem('partie'));
    document.getElementById('part1').style.backgroundColor = partie.color1;
    document.getElementById('part2').style.backgroundColor = partie.color2;
    document.getElementById('part3').style.backgroundColor = partie.color3;


  }
  initdrapeaux();

}



let drapaux = [
  {
    pays: "France",
    poistion: "vertical",
    nombre_part: 3,
    colorOrder: [
      'blue',
      'white',
      'red'
    ],
  },
  {
    pays: "Belgique",
    poistion: "vertical",
    nombre_part: 3,
    colorOrder: [
      "black",
      "yellow",
      "red",
    ],
  },
  {
    pays: "Allemagne",
    poistion: "horizontal",
    nombre_part: 3,
    colorOrder: [
      "black",
      "red",
      "yellow",
    ],
  },
  {
    pays: "Pays-Bas",
    poistion: "horizontal",
    nombre_part: 3,
    colorOrder: [
      "red",
      "white",
      "blue",
    ],

  },
  {
    pays: "Pologne",
    poistion: "horizontal",
    nombre_part: 2,
    colorOrder: [
      "white",
      "red"
    ],
  },
  {
    pays: "République-tchèque",
    poistion: "triangle",
    nombre_part: 3,
    colorOrder: [
      "white",
      "blue",
      "red"
    ]
  }

];




Array.prototype.forEach.call(falgPart, function (el) {

  el.title = -1;
  el.addEventListener('click', clickEvent)
})


function clickEvent(e) {

  colorPick(e.target);

  if (checkVictory(falgPart)) {

    CalculScore();
    partie.scoreFinal += partie.score;

    if( partie.level < drapaux.length){
      setTimeout(() => { popupVictoire(); }, 500);
     
    }

    partie.level++
    resetJoker();

    setTimeout(() => { initdrapeaux(); }, 1000);
    resetFlag();
    resetTimer();
    startTimer();

  }

}


function CalculScore() {

  if (partie.level === 0) {

    if (partie.click < 6 && partie.s < 4) {
    
      partie.score = 3;
    } else if (partie.s < 4) {
    
      partie.score = 2;
    } else {
    
      partie.score = 1;
    }
  }

  else if (partie.level === 1) {
    if (partie.click < 9 && partie.s < 7) {
      partie.score = 3;
    } else if (partie.s < 7) {
      partie.score = 2;
    } else {
      partie.score = 1;
    }
  }

  else if (partie.level === 2) {
    if (partie.click < 9 && partie.s < 7) {
      partie.score = 3;
    } else if (partie.s < 7) {
      partie.score = 2;
    } else {
      partie.score = 1;
    }
  }

  else if (partie.level === 3) {
    if (partie.click < 9 && partie.s < 7) {
      partie.score = 3;
    } else if (partie.s < 7) {
      partie.score = 2;
    } else {
      partie.score = 1;
    }
  }

  else if (partie.level === 4) {
    if (partie.click < 3 && partie.s < 1) {
      partie.score = 3;
    } else if (partie.s < 1) {
      partie.score = 2;
    } else {
      partie.score = 1;
    }
  }

  else if (partie.level === 5) {
    if (partie.click < 5 && partie.s < 3) {
      partie.score = 3;
    } else if (partie.s < 3) {
      partie.score = 2;
    } else {
      partie.score = 1;
    }
  }
}


/** fonction initialisation drapeaux */
function initdrapeaux() {

  if (partie.level < drapaux.length) {

 
    colors = drapaux[partie.level].colorOrder.reverse().slice();
    drapaux[partie.level].colorOrder.reverse();
    document.getElementById('compteNiveau').textContent = partie.level + 1;
    document.getElementById('score').textContent = partie.scoreFinal;
    pays.textContent = drapaux[partie.level].pays;

    changeShap();

  } else {
    setTimeout(() => { popupFin(); }, 2000);
    localStorage.clear();
    partie.level = 0;
    partie.scoreFinal = 0;
    partie.clickTotal = 0;
  }
}

function resetJoker(){

  partie.joker = true;
  falgPart.item(0).style.pointerEvents = "auto";
  document.getElementById("boutonJoker").disabled = false;

}

function joker(){

  falgPart.item(0).style.backgroundColor = drapaux[partie.level].colorOrder[0];
  partie.color1 =  falgPart.item(0).style.backgroundColor;

  document.getElementById("boutonJoker").disabled = true;

  falgPart.item(0).style.pointerEvents = "none";

  partie.joker = false;
  partie.score -= 1;

}

/** fonction pour reset les couleurs du drapeaux */
function resetFlag() {

  for (let i = 0; i < falgPart.length; i++) {
    falgPart.item(i).style.backgroundColor = "aliceblue";

  }

  partie.color1 = "aliceblue";
  partie.color2 = "aliceblue";
  partie.color3 = "aliceblue";

}

/** fonction pour changer de couleur */
function colorPick(el) {


  el.title = (el.title >= colors.length - 1) ? 0 : parseInt(el.title) + 1;
  el.style.backgroundColor = colors[el.title];

  switch (el.id) {

    case "part1": partie.color1 = el.style.backgroundColor; break;
    case "part2": partie.color2 = el.style.backgroundColor; break;
    case "part3": partie.color3 = el.style.backgroundColor; break;
  }

  partie.click++;
  partie.clickTotal++;
  document.getElementById('compteClic').textContent = partie.click;

}


function checkVictory(falgPart) {



  for (let i = 0; i < drapaux[partie.level].colorOrder.length; i++) {

    if (!(falgPart.item(i).style.backgroundColor === drapaux[partie.level].colorOrder[i])) {


      return false;

    }

  }
  return true;
}

function changeShap() {

  if (drapaux[partie.level].poistion === "horizontal") {

    horizontalShap()

    if (drapaux[partie.level].nombre_part < 3) {

      falgPart.item(2).style.visibility = 'hidden';

    } else {

      falgPart.item(2).style.visibility = 'visible';

    }
  }
  else if (drapaux[partie.level].poistion === "vertical") {

    verticalShap()

  } else if (drapaux[partie.level].poistion = "triangle") {

    falgPart.item(2).classList.add('partDown');
    falgPart.item(0).classList.add('partTop');
    falgPart.item(1).classList.add('triangle');
    falgPart.item(2).style.visibility = 'visible';

    for (let i = 0; i < falgPart.length; i++) {

      falgPart.item(i).style.padding = "0";
      falgPart.item(i).style.height = "28%";

    }

  }
}

function horizontalShap() {

  document.getElementById('container').style.flexDirection = 'column';

  for (let i = 0; i < falgPart.length; i++) {

    falgPart.item(i).style.padding = "10% 0";
    falgPart.item(i).style.height = "5%";

  }

}

function verticalShap() {

  document.getElementById('container').style.flexDirection = 'row';

  for (let i = 0; i < falgPart.length; i++) {

    falgPart.item(i).style.padding = "20% 0";

  }
}


let timer;

function startTimer() {

  timer = setInterval(function () {

    if (partie.s < 59) {
      partie.s += 1;
    } else if (partie.m < 59) {
      partie.m += 1
      partie.s = 0;
    } else {
      partie.h += 1
      partie.m = 0;
    }
    document.getElementById("tempsTimer").innerHTML = partie.h + " : "
      + partie.m + " : " + partie.s;
  }, 1000);
}

function resetTimer() {
  partie.s = -1;
  partie.m = 0;
  partie.h = 0;
  document.getElementById("timer").innerHTML = partie.h + " : "
    + partie.m + " : " + partie.s;
}

function restartTimer() {
  startTimer();
}





function commencer() {
  document.getElementById("divAccueil").style.display = "none";
  document.getElementById("game").style.display = "block";
  startTimer();
}

function popupVictoire() {

  document.querySelector("#statsScore").innerHTML = "Nombre de points : " + partie.score;
  document.querySelector("#statsClick").innerHTML = "Nombre de cliques : " + partie.click;
  let affichage = document.getElementById("ecranVictoire");
  affichage.style.display = "block";

  span.onclick = function () {
    affichage.style.display = "none";
  }

  window.onclick = function (event) {
    if (event.target == affichage) {
      affichage.style.display = "none";
    }
  }
}

function popupFin() {

  document.querySelector("#statsClick2").innerHTML = "Nombre de clique : " + partie.clickTotal
  document.querySelector("#statsScore2").innerHTML = "Score finale : " + partie.scoreFinal;
  let affichage = document.getElementById("ecranFin");
  affichage.style.display = "block";

}

window.onbeforeunload = function () {

  if (partie.level < drapaux.length) {

    localStorage.setItem("partie", JSON.stringify(partie))
  }else{
    localStorage.clear();
  }
}

function abandonner() {

  partie.scoreFinal = partie.scoreFinal - 1;
  partie.score = 0;

  if( partie.level < drapaux.length ){
    setTimeout(() => { popupVictoire(); }, 500);
  }

  partie.level++;

  setTimeout(() => { initdrapeaux(); }, 1000);
  resetFlag();
  resetTimer();
  startTimer();
}

function rejouer() {
  partie.s = 0;
  partie.m = 0;
  partie.h = 0;
  partie.click = 0;
  partie.clickTotal = 0;
  partie.score = 0;
  partie.scoreFinal = 0;
  window.location.reload();
}
